# ZAPI Maven Plugin

**Current Version: 0.1**

The zapi-maven-plugin Plugin add capability to update test results from [surefire](http://maven.apache.org/surefire/maven-surefire-plugin/) and [failsafe](https://maven.apache.org/surefire/maven-failsafe-plugin/) reports.

For more information and usage guide, [see the wiki](https://bitbucket.org/zfjdeveloper/zapi-maven-plugin/wiki/Home)

To log an issue or feature request, [use the issue tracker](https://bitbucket.org/zfjdeveloper/zapi-maven-plugin/issues)

Got something to say?  [@yourzephyr](https://twitter.com/yourzephyr) #zapi-maven-plugin