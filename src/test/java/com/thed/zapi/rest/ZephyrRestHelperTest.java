package com.thed.zapi.rest;

import com.thed.zapi.rest.config.Configuration;
import com.thed.zapi.rest.vo.StatusVO;
import org.codehaus.jettison.json.JSONException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by smangal on 1/30/14.
 */
public class ZephyrRestHelperTest {
    private Configuration config;
    private ZapiRestHelper impl;
    private String fieldVal;

//    @Before
//    public void setUp(){
//        config = new Configuration("http://192.168.11.71:1212/jira/", "vm_admin", "minimini", 1, "10200", "10300");
//        impl = new ZapiRestHelper(config);
//        fieldVal = "com.thed.test.ZephyrRestHelperTest" + System.currentTimeMillis();
//    }

    @Test
    public void createCycle() throws JSONException {
        // Please, do not remove this line from file template, here invocation of web service will be inserted
        AbstractCollection<Long> cycles = new ArrayList<Long>(100);
        for (int i = 0; i < 1; i++) {
            Long cycleId = impl.createCycle("UTCycle");
            assertNotNull(cycleId);
            cycles.add(cycleId);
        }

        Iterator<Long> it = cycles.iterator();
        while (it.hasNext()) {
            impl.deleteCycle(it.next());
        }
    }

    @Test
    public void getExecutionStatus(){
        List<StatusVO> statuses = impl.getExecutionStatuses();
        assertNotNull(statuses);
        assertTrue(statuses.size() > 0);

        List<StatusVO> stepStatuses = impl.getStepStatuses();
        assertNotNull(stepStatuses);
        assertTrue(stepStatuses.size() > 0);
    }

    @Test
    public void executeTest()  throws JSONException, IOException{
        Long expectedTestcaseId = impl.findOrAddTestcase("summary", fieldVal);
        Long cycleId = impl.createCycle("UTCycle");
        impl.executeTest(cycleId.intValue(), expectedTestcaseId.intValue(), "2", "Fake Comments");
        impl.deleteCycle(cycleId);
    }

    @Test
    public void findOrAddTestcase() throws JSONException, IOException {
        Long expectedTestcaseId = impl.findOrAddTestcase("summary", fieldVal);
        assertNotNull(expectedTestcaseId);
        assertTrue(expectedTestcaseId > 0);
    }
    
    @Test
    public void deleteTestcase() throws JSONException, IOException {
    	assertTrue(2 > 4);
    }
    
    @Test
    public void createTeststeps() throws JSONException, IOException {
    	assertTrue(2 > 4);
    }

    @Test
    public void findTeststeps() throws JSONException, IOException {
    	assertTrue(2 > 4);
    }

    @Test
    public void updateTeststeps() throws JSONException, IOException {
    	assertTrue(2 > 4);
    }

    @Test
    public void test1() throws JSONException, IOException {
    	assertTrue(6 > 4);
    }

    @Test
    public void test2() throws JSONException, IOException {
    	assertTrue(6 > 4);
    }

    @Test
    public void test3() throws JSONException, IOException {
    	assertTrue(6 > 4);
    }
    
    @Test
    public void test4() throws JSONException, IOException {
    	assertTrue(6 > 4);
    }

    @Test
    public void test5() throws JSONException, IOException {
    	assertTrue(6 > 4);
    }


}
