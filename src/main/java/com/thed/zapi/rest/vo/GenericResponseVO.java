package com.thed.zapi.rest.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by smangal on 1/31/14.
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenericResponseVO {
        @XmlElement
        public Long id;
        @XmlElement
        public String responseMessage;

        public GenericResponseVO()
        {
        }

    public Long getId() {
        return id;
    }
}
